from typing import Generator

from letters import lowercase, uppercase


def encrypt(
        phrase: str,
        shift: int
) -> str:
    return ''.join(
        _encrypt_each_letter(
            phrase,
            shift
        )
    )


def _encrypt_each_letter(
        phrase: str,
        shift: int
) -> Generator:
    return (
        _encrypt_letter(
            letter,
            shift
        )
        for letter in phrase
    )


def _encrypt_letter(
        letter: str,
        shift: int
) -> str:
    if letter in lowercase:
        return _get_new_lowercase_letter(
            letter,
            shift
        )
    elif letter in uppercase:
        return _get_new_uppercase_letter(
            letter,
            shift
        )
    else:
        raise_error(letter)


def raise_error(letter):
    raise RuntimeError(f"Letter '{letter}' is not supported")


def _get_new_lowercase_letter(
        letter: str,
        shift: int
) -> str:
    return _get_new_letter(
        letter,
        lowercase,
        shift
    )


def _get_new_uppercase_letter(
        letter: str,
        shift: int
) -> str:
    return _get_new_letter(
        letter,
        uppercase,
        shift
    )


def _get_new_letter(
        letter: str,
        all_letters: str,
        shift: int
) -> str:
    current_position = all_letters.index(letter)
    shifted = current_position + shift
    alphabet_length = len(all_letters)
    new_position = shifted % alphabet_length
    return all_letters[new_position]
