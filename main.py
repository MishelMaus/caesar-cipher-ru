from bottle import post, run, request

import logic


@post("/encrypt")
def encrypt():
    phrase, shift = _get_request_data()
    encrypted = logic.encrypt(
        phrase,
        shift
    )
    return {
        'original': phrase,
        'encrypted': encrypted,
        'shift': shift
    }


def _get_request_data() -> (str, int):
    data = request.json
    return data['phrase'], data['shift']


run(host='localhost', port=8080)
