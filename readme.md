# Caesar cipher
Implements [Caesar cipher](https://en.wikipedia.org/wiki/Caesar_cipher) for [Russian alphabet](https://en.wikipedia.org/wiki/Russian_alphabet)

## Requirements
See `requirements.txt`

## Usage
1. Launch `main.py`
2. The API is available at `http://localhost:8080/encrypt`
3. Make a POST JSON request to it:

        {
          "phrase": <string>,
          "shift": <integer>
        }

## Example
### Request

    {
      "phrase": "МаМа",
      "shift": 1
    }
### Response

    {
      "original": "МаМа",
      "encrypted": "НбНб",
      "shift": 1
    }