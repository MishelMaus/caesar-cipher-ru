from unittest import TestCase
from logic import encrypt


class Test(TestCase):
    def test_encrypt(self):
        to_encrypt = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ'

        encrypted_1 = encrypt(to_encrypt, 0)
        encrypted_2 = encrypt(to_encrypt, -1)
        encrypted_3 = encrypt(to_encrypt, -42)
        encrypted_4 = encrypt(to_encrypt, 1)
        encrypted_5 = encrypt(to_encrypt, 42)

        self.assertEqual('абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ', encrypted_1)
        self.assertEqual('яабвгдеёжзийклмнопрстуфхцчшщъыьэюЯАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮ', encrypted_2)
        self.assertEqual('чшщъыьэюяабвгдеёжзийклмнопрстуфхцЧШЩЪЫЬЭЮЯАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦ', encrypted_3)
        self.assertEqual('бвгдеёжзийклмнопрстуфхцчшщъыьэюяаБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯА', encrypted_4)
        self.assertEqual('ийклмнопрстуфхцчшщъыьэюяабвгдеёжзИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯАБВГДЕЁЖЗ', encrypted_5)

    def test_encrypt_validation(self):
        to_encrypt = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ '

        with self.assertRaises(RuntimeError) as error_context:
            encrypt(to_encrypt, 42)

        exception = error_context.exception
        self.assertEqual("Letter ' ' is not supported", exception.args[0])
